﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace oefMethodesWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private bool MethodKapitaalcheck ()
        {
            bool isKapitaal = double.TryParse(txtKapitaal.Text, out double kapitaal);

            return (kapitaal > 0);
        }
        private string MethodStringJaar (out double volgendJaar)
        {
            bool isKapitaal = double.TryParse(txtKapitaal.Text, out double kapitaal);
            bool isRente = double.TryParse(txtRente.Text, out double rente);
            volgendJaar = (kapitaal / 100) * rente;
           

        }

        private void Bereken_Click(object sender, RoutedEventArgs e)
        {
            if (MethodKapitaalcheck())
            {
                txtKapitaal.Background = Brushes.LimeGreen;
            }


            txtVolgendJaar.Text = "Dag " + Convert.ToString(txtNaam.Text) + "\n" +
                "Je zal volgend jaar " + volgendJaar()+ " euro rente ontvangen.";
            
        }

        private void Wissen_Click(object sender, RoutedEventArgs e)
        {
            txtNaam.Clear();
            txtKapitaal.Clear();
            txtRente.Clear();
            txtVolgendJaar.Clear();
        }

        private void Afsluiten_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
