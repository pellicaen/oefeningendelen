﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TussentijdseTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Randombackground();
            
        }
        private void Randombackground()
        {
            Background = Brushes.Green;
        }

        private void btnVoegToe_Click(object sender, RoutedEventArgs e)
        {
            string stringProductNaam = Convert.ToString(txtProductNaam.Text);
            string stringPrijs = Convert.ToString(txtNieuweBestelling.Text);
            double doublePrijs;
            bool isDoubleprijs = double.TryParse(stringPrijs, out doublePrijs);
            string stringHoeveelheid = Convert.ToString(txtHoeveelheid.Text);
            double doubleHoeveelheid;
            bool isHoeveelheid = double.TryParse(stringPrijs, out doubleHoeveelheid);
            string NieuweLijn = doubleHoeveelheid + " x " + stringProductNaam;

            double doubleTotaalprijs = doubleHoeveelheid * doublePrijs;
            double Totaalprijs = doubleTotaalprijs + doubleTotaalprijs;

            
        }

        private void btnNieuweBestelling_Click(object sender, RoutedEventArgs e)
        {
            txtHoeveelheid.Clear();
            txtNieuweBestelling.Clear();
            txtProductNaam.Clear();
            txtProductPrijs.Clear();
        }
    }
}
